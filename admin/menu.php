<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$menu = [
    [
        'parent_menu' => 'global_menu_content',
        'sort' => 400,
        'text' => Loc::getMessage('MYTEST_AGENT_MENU_TITLE'),
        'title' => Loc::getMessage('MYTEST_AGENT_MENU_TITLE'),
        'items_id' => 'menu_references',
        'items' => [
            [
                'text' => Loc::getMessage('MYTEST_AGENT_SUBMENU_TITLE'),
                'title' => Loc::getMessage('MYTEST_AGENT_SUBMENU_TITLE'),
                'url' => 'perfmon_table.php?lang=ru&table_name=mytest_agent',
            ],
        ],
    ],
];

return $menu;