<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\LanguageTable;
use Bitrix\Main\Mail\Internal\EventTypeTable;
use Bitrix\Main\Mail\Internal\EventMessageTable;
use Bitrix\Main\Mail\Internal\EventMessageSiteTable;
use Bitrix\Main\Context;
use Bitrix\Main\SiteTable;
use Bitrix\Main\EventManager;
use Mytest\Agent\DataTable;
use Mytest\Agent\Main\Cron;

Loc::loadMessages(__FILE__);

class Mytest_Agent extends CModule
{
    public function __construct()
    {
        $arModuleVersion = include __DIR__ . '/version.php';
        
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        
        $this->MODULE_ID = 'mytest.agent';
        
        $this->MODULE_NAME = Loc::getMessage('MYTEST_AGENT_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MYTEST_AGENT_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('MYTEST_AGENT_MODULE_PARTNER_NAME');
    }
    
    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        
        $this->installDB();
        
        \CAgent::AddAgent(
            '\Mytest\Agent\Main\Cron::agent();',
            $this->MODULE_ID,
            'N',
            3600
//            15
        );
        
        $this->installMailEvent();
        
        EventManager::getInstance()->registerEventHandler(
            'sale',
            'OnSaleStatusOrderChange',
            $this->MODULE_ID,
            '\Mytest\Agent\Main\Events',
            'onOrderStatusChange'
        );
    }
    
    public function doUninstall()
    {
        $this->uninstallDB();
        
        \CAgent::RemoveAgent('\Mytest\Agent\Main\Cron::agent();', $this->MODULE_ID);
        
        $this->uninstallMailEvent();
        
        EventManager::getInstance()->unRegisterEventHandler(
            'sale',
            'OnSaleStatusOrderChange',
            $this->MODULE_ID,
            '\Mytest\Agent\Main\Events',
            'onOrderStatusChange'
        );
        
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
    
    public function installMailEvent()
    {
        $rsElements = LanguageTable::getList([
            'select' => [
                'LID',
            ],
        ]);

        while ($arElement = $rsElements->fetch()) {

            EventTypeTable::add([
                'LID' => $arElement['LID'],
                'EVENT_NAME' => Cron::EVENT_NAME,
                'NAME' => Loc::getMessage('MYTEST_AGENT_EVENT_TYPE_NAME'),
                'DESCRIPTION' => Loc::getMessage('MYTEST_AGENT_EVENT_TYPE_DESCRIPTION'),
                'EVENT_TYPE' => EventTypeTable::TYPE_EMAIL,
            ]);
        }

        $addResult = EventMessageTable::add([
            'EVENT_NAME' => Cron::EVENT_NAME,
            'LID' => Context::getCurrent()->getSite(),
            'SUBJECT' => Loc::getMessage('MYTEST_AGENT_EMAIL_MESSAGE_SUBJECT'),
            'MESSAGE' => Loc::getMessage('MYTEST_AGENT_EMAIL_MESSAGE_BODY'),
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO' => '#DEFAULT_EMAIL_FROM#',
            'BODY_TYPE' => 'html',
        ]);

        $rsElements = SiteTable::getList([
            'select' => [
                'LID',
            ],
        ]);
        
        while ($arElement = $rsElements->fetch()) {
            EventMessageSiteTable::add([
                'EVENT_MESSAGE_ID' => $addResult->getId(),
                'SITE_ID' => $arElement['LID'],
            ]);
        }
    }
    
    public function uninstallMailEvent()
    {
        $rsElements = EventTypeTable::getList([
            'select' => [
                'ID',
            ],
            'filter' => [
                'EVENT_NAME' => Cron::EVENT_NAME,
            ],
        ]);

        while ($arElement = $rsElements->fetch()) {
            EventTypeTable::delete($arElement['ID']);
        }

        $rsElements = EventMessageTable::getList([
            'select' => [
                'ID',
            ],
            'filter' => [
                'EVENT_NAME' => Cron::EVENT_NAME,
            ],
        ]);

        while ($arElement = $rsElements->fetch()) {
            EventMessageTable::delete($arElement['ID']);
            EventMessageSiteTable::delete($arElement['ID']);
        }
    }
    
    public function installDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            DataTable::getEntity()->createDbTable();
        }
    }
    
    public function uninstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            if (Application::getConnection()->isTableExists(Base::getInstance('\Mytest\Agent\DataTable')->getDBTableName())) {
                $connection = Application::getInstance()->getConnection();
                $connection->dropTable(DataTable::getTableName());
            }
        }
    }
}