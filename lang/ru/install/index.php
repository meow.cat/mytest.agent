<?php

$MESS['MYTEST_AGENT_MODULE_NAME'] = 'Тестовое задание 1';
$MESS['MYTEST_AGENT_MODULE_DESCRIPTION'] = 'Заказы, которые висели в начальном статусе больше двух дней';
$MESS['MYTEST_AGENT_MODULE_PARTNER_NAME'] = 'Фыр-Фыр ʕ ᵔᴥᵔ ʔ';
$MESS['MYTEST_AGENT_EVENT_TYPE_NAME'] = 'Заказы, которые висят в начальном статусе больше двух дней';
$MESS['MYTEST_AGENT_EVENT_TYPE_DESCRIPTION'] = '#IDS# - ID заказов';
$MESS['MYTEST_AGENT_EMAIL_MESSAGE_SUBJECT'] = 'Заказы, которые висят в начальном статусе больше двух дней';
$MESS['MYTEST_AGENT_EMAIL_MESSAGE_BODY'] = '<b>ID заказов, которые висят в начальном статусе больше двух дней</b>: #IDS#';
