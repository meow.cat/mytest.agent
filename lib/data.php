<?php

namespace Mytest\Agent;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Type;

class DataTable extends DataManager
{
    public static function getTableName()
    {
        return 'mytest_agent';
    }

    public static function getMap()
    {
        return [
            'ORDER_ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'required' => true,
            ],
            'DATE_INSERT' => [
                'data_type' => 'datetime',
                'required' => true,
                'default_value' => function(){return new Type\DateTime();},
            ],
        ];
    }
}