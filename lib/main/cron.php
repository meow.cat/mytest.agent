<?php

namespace Mytest\Agent\Main;

use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Context;
use Bitrix\Main\ORM\Query\Query;
use Bitrix\Sale\Order;
use Mytest\Agent\DataTable;

class Cron
{
    const COUNT_DAYS = 2;
    
    const EVENT_NAME = 'MYTEST_AGENT_CHECK_ORDER_STATUS';
    
    public static function agent()
    {
        static::check();
        return '\\' . __CLASS__ . '::' . __FUNCTION__ . '();';
    }
    
    public static function check()
    {
        if (!Loader::includeModule('sale')) {
            return;
        }

        $filter = Query::filter();
        $filter->where('STATUS_ID', 'N');
//        $filter->where('DATE_STATUS', '<=', new DateTime());
        $filter->where('DATE_STATUS', '<=', (new DateTime())->add(sprintf('-%d day', static::COUNT_DAYS)));
        $filter->whereNotIn('ID', DataTable::query()->setSelect(['ORDER_ID']));
        
        $objOrdersRes = Order::getList([
            'select' => ['ID'],
            'filter' => $filter,
        ]);

        $arIds=[];

        while ($order = $objOrdersRes->fetch()) {
            $arIds[] = $order['ID'];
        }
        
        if (!$arIds) {
            return;
        }

        foreach ($arIds as $orderId) {
            DataTable::add([
                'ORDER_ID' => $orderId,
            ]);
        }
        
        Event::sendImmediate([
            'EVENT_NAME' => static::EVENT_NAME,
            'LID' => Context::getCurrent()->getSite(),
            'C_FIELDS' => [
                'IDS' => implode(', ', $arIds),
            ],
        ]);
    }
}