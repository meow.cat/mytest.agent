<?php

namespace Mytest\Agent\Main;

use Bitrix\Main\EventResult;
use Mytest\Agent\DataTable;

class Events
{
    public static function onOrderStatusChange($event)
    {
        $parameters = $event->getParameters();
        
        DataTable::delete($parameters['ENTITY']->getId());

        return new EventResult(EventResult::SUCCESS);
    }
}